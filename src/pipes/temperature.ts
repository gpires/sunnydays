import { Injectable, Pipe } from '@angular/core';

/*
  Generated class for the Temperature pipe.

  See https://angular.io/docs/ts/latest/guide/pipes.html for more info on
  Angular 2 Pipes.
*/
@Pipe({
  name: 'temperature'
})
@Injectable()
export class Temperature {
  /*
    Takes a value and makes it lowercase.
   */
  transform(value: string, args?: any[]) {
    var c = Math.round(parseInt(value, 10) - 273.15);
    //var f = Math.round(parseInt(value, 10) * 9 / 5 - 459.67);
    return `${c} °C`;
  }

}
