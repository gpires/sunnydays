import { Component, Input, Output, EventEmitter } from '@angular/core';

/*
  Generated class for the WeatherEl component.

  See https://angular.io/docs/ts/latest/api/core/index/ComponentMetadata-class.html
  for more info on Angular 2 Components.
*/

@Component({
  selector: 'weather-el',
  templateUrl: 'weather-el.html',

})

export class WeatherEl {

  @Input() weather: Object;
  @Output() viewMore: EventEmitter<Object> = new EventEmitter();

  constructor() {}

    hitWeather(){
      this.viewMore.next(this.weather);
    
    }
}
