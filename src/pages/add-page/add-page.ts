import { Component } from '@angular/core';
import { NavController, ViewController } from 'ionic-angular';


@Component({
  selector: 'page-add-page',
  templateUrl: 'add-page.html'
})
export class AddPage {

  public data = {
    city: '',
    country: 'pt'
  }

  constructor(public navCtrl: NavController,
    public viewCtrl: ViewController) { }

  logform() {
    this.viewCtrl.dismiss(this.data);
  }

}