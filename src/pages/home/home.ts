import { Component } from '@angular/core';
import { NavController, ModalController, AlertController } from 'ionic-angular';
import {AddPage} from '../add-page/add-page';
import { Weather } from '../../providers/weather';
import 'rxjs/add/operator/map';
//import { Temperature } from '../../pipes/temperature';
import {Observable} from 'rxjs/Observable';
import 'rxjs/Rx';
import { Forecast } from '../forecast/forecast';
import { StorageProvider } from '../../providers/storage';



@Component({
  selector: 'page-home',
  templateUrl: 'home.html',
  providers: [Weather]

})
export class HomePage {

  public weatherList: any =  [];

  public localWeather;

  constructor(public navCtrl: NavController,
    public modalCtrl: ModalController,
    public weather: Weather,
    public alertCtrl: AlertController,
    public storage: StorageProvider
  ) {
    this.getLocalWeather();
    this.getStoredWeather();
  }


  getStoredWeather() {
    this.storage.getWeather().then((data) => {
      this.weatherList.push(data);
    });

  }

  addWeather() {
    let addWeatherModal = this.modalCtrl.create(AddPage);
    addWeatherModal.onDidDismiss((data) => {

      if (data) {
        this.getWeather(data.city, data.country);
      }
    });
    addWeatherModal.present(AddPage);
  }

  getWeather(city: string, country: string) {
    this.weather.city(city, country)
      .map(data => data.json())
      .subscribe(data => {
        this.weatherList.push(data);
        this.storage.setWeather('Database', data);
      },
      err => console.log(err),
      () => console.log('Weather Here'))
  }

  getLocalWeather() {
    this.weather.local().subscribe(
      data => {

        this.localWeather = data;
      },
      err => console.log(err),
      () => console.log('local weather complete')
    )

  }

  viewForecast(cityWeather) {
    this.navCtrl.push(Forecast, { cityWeather: cityWeather });
  }



}
