//import {NavParams} from 'ionic-angular/umd';
import { Component } from '@angular/core';
import { NavController, NavParams } from 'ionic-angular';
import { Weather } from '../../providers/weather';
import { Temperature } from '../../pipes/temperature';

/*
  Generated class for the Forecast page.

  See http://ionicframework.com/docs/v2/components/#navigation for more info on
  Ionic pages and navigation.
*/
@Component({
  selector: 'page-forecast',
  templateUrl: 'forecast.html'
})
export class Forecast {
  
  public cityWeather;
  public forecast = [];

  constructor(public navCtrl: NavController,
    public navParams: NavParams,
    public weather: Weather) {
    this.cityWeather = navParams.get('cityWeather');
    this.getForecast(this.cityWeather.id);

  }

  getForecast(cityId) {
    this.weather.forecast(cityId)
      .map(data => data.json())
      .subscribe(data => {
        this.forecast = data.list;
      },
      err => console.log(err),
      () => console.log('forecast complete')
      )
  }

}
