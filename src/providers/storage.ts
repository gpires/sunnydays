

import { Injectable } from '@angular/core';
import 'rxjs/add/operator/map';
import { Storage } from '@ionic/storage';





@Injectable()
export class StorageProvider {


  private storageDb = "weatherStorage";

  //public weather: Array<Object>

  constructor(private storage: Storage) { }

  setWeather(storageDb: string, city: string) {
    this.storage.set(this.storageDb, city);
  }

  getWeather() {
    return this.storage.get(this.storageDb);
  }
}
