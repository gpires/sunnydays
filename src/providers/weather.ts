
import { Injectable } from '@angular/core';
import { Http, Response } from '@angular/http';
import 'rxjs/add/operator/map';
import { Observable } from 'rxjs/Observable';
import { Geolocation } from 'ionic-native';
import {Platform} from 'ionic-angular';





@Injectable()
export class Weather {

  private appID = '4a69939514e751afd189bc7ff6d66290';
  private baseUrl = 'http://api.openweathermap.org/data/2.5/';

  constructor(public http: Http, public platform: Platform) { }

  city(city: string, country: string) {

    let url = this.baseUrl + 'weather';
    url += '?appID=' + this.appID;
    url += '&q=' + city;
    url += ',' + country;
    return this.http.get(url);

  }

  forecast(cityId: string) {

    let url = this.baseUrl + 'forecast';
    url += '?id=' + cityId;
    url += '&appID=' + this.appID;

    return this.http.get(url);
  }

  local() {
    let Obs = Observable.create(observer => {

      this.platform.ready().then(() => {

        Geolocation.getCurrentPosition({ timeout: 10000, enableHighAccuracy: true })
          .then((resp) => {
            let lat = resp.coords.latitude;
            let lng = resp.coords.longitude;

            let url = this.baseUrl + 'weather';
            url += `?lat=${lat}&lon=${lng}`;
            url += '&appID=' + this.appID;

            console.log(url);

            this.http.get(url)
              .subscribe(
              data => {
                console.log(data);
                observer.next(data.json());
              },
              err => observer.error(err),
              () => observer.complete()
              )
          }, (err) => {
            console.log(err);
            observer.error(err);
            observer.complete();
          });

      });

    });

    return Obs;





  }
}



