import { NgModule } from '@angular/core';
import { IonicApp, IonicModule } from 'ionic-angular';
import { MyApp } from './app.component';
import { HomePage } from '../pages/home/home';
import { AddPage } from '../pages/add-page/add-page';
import { Weather } from '../providers/weather';
import { Temperature } from '../pipes/temperature';
import { Forecast } from '../pages/forecast/forecast';
import { WeatherEl } from '../components/weather-el/weather-el';
import { Storage } from '@ionic/storage';
import { StorageProvider } from '../providers/storage';



@NgModule({
  declarations: [
    MyApp,
    HomePage,
    AddPage,
    Temperature,
    Forecast,
    WeatherEl
  ],
  imports: [
    IonicModule.forRoot(MyApp)
  ],
  bootstrap: [IonicApp],
  entryComponents: [
    MyApp,
    HomePage,
    AddPage,
    Forecast,
    
  ],
  providers: [Weather, Storage, StorageProvider]
  
})
export class AppModule {}
